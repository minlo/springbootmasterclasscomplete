package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class IDepartmentRepositoryTest {

    @Autowired
    private IDepartmentRepository departmentRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentName("Bank-Verlag")
                .departmentAddress("Musterstr 3")
                .departmentCode("9999")
                .build();

        entityManager.persist(department);
    }

    @Test
    public void shouldReturnDepartmentWhenFindById(){
        final Department department = departmentRepository.findById(1L).get();

        assertEquals(department.getDepartmentName(), "Bank-Verlag");


    }
}