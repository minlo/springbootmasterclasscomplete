package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Course;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.Student;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;


@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void printCourses(){

        final List<Course> courseList = courseRepository.findAll();
        System.out.println("All Course "+courseList);

    }

    @Test
    public void saveCourse(){
        Student student = Student.builder()
                .firstname("David")
                .lastname("Minlo")
                .emailId("email1@Mailto.de")
                .build();

        Teacher teacher =Teacher.builder()
                .fisrtname("Frank")
                .lastname("Wolfgang")
                .email("wolfgang@t-online.de")
                .build();

        Course p1 = Course.builder()
                .title("Programmierung I")
                .credit(6)
                .teacher(teacher)
                .build();

        Course p2 = Course.builder()
                .title("Programmierung II")
                .credit(6)
                .students(List.of(student))
                .teacher(teacher)
                .build();
        courseRepository.save(p1);
      //courseRepository.save(p2);
    }

    @Test
    public void findAllPagination(){

        Pageable firstPageWithThreeRecords = PageRequest.of(0, 3);
        Pageable secondPageWithTwoRecords =  PageRequest.of(1, 2);

        final List<Course> courses = courseRepository.findAll(firstPageWithThreeRecords).getContent();

        System.out.println("FirstPageWithThreeRecords "+courses);


    }

    @Test
    public void saveCourseWithStudentAndTeacher(){
        Student jojo = Student.builder()
                .emailId("joel@tu-bs.de")
                .firstname("Joseph")
                .lastname("Minlo")
                .build();

        Student grosNoyaux = Student.builder()
                .emailId("grosnoyaux@tu-bs.de")
                .firstname("Fabrice")
                .lastname("Fokam")
                .build();

        Student deserteur = Student.builder()
                .emailId("deserteur@tu-bs.de")
                .firstname("Patson")
                .lastname("Takam")
                .build();

        Teacher teacher = Teacher.builder()
                .fisrtname("Müller")
                .lastname("Strukmann")
                .email("minko@mailto.com")
                .build();

        Course programmieren1 = Course.builder()
                .title("Programmierung 1")
                .credit(6)
                .teacher(teacher)
                //.students(List.of(jojo, grosNoyaux, deserteur))
                .build();

        programmieren1.addStudent(jojo);
        //programmieren1.addStudent(grosNoyaux);
        //programmieren1.addStudent(deserteur);

        courseRepository.save(programmieren1);



    }

}