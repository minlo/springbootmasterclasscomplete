package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Guardian;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IStudentRepositoryTest {

    @Autowired
    private IStudentRepository studentRepository;

    @Test
    public void saveStudent(){
        //To save a student, you need to create the student first
        Student student = Student.builder()
                .emailId("joel@minlo.de")
                .firstname("JoJo")
                .lastname("Minlo")
                .build();
        // now we can save student-object in our DB
         studentRepository.save(student);
    }

    @Test
    public void saveStudentWithGuardian(){

        Guardian guardian = Guardian.builder()
                .name("Guardiansname")
                .email("guardian@yahoo.com")
                .mobile("123456")
                .build();

        Student student = Student.builder()
                .emailId("jojo@minlo.com")
                .firstname("Dukenzo")
                .lastname("Mika")
                .guardian(guardian)
                .build();
        studentRepository.save(student);
    }

    @Test
    public void allStudent(){
        final List<Student> studentList = studentRepository.findAll();

        System.out.println("List all students "+studentList);
    }

    @Test
    public void returnStudentById(){

        final Optional<Student> studentOptional = studentRepository.findById(1L);
        assertEquals(studentOptional.get().getLastname(), "Minlo");
        assertEquals(studentOptional.get().getFirstname(), "JoJo");


    }

    @Test
    public void shouldReturnStudentsByFirstname(){
        final List<Student> byFirstname = studentRepository.findByFirstname("firstname");
        System.out.println("StudentByFirstname: "+byFirstname);

    }

    @Test
    public void shouldReturnStudentByEmailAddress(){

        final Student emailAddress = studentRepository.getStudentByEmailAddress("joel@minlo.de");
        assertEquals(emailAddress.getEmailId(), "joel@minlo.de");
    }

    @Test
    public void shouldReturnFirstnameByEmail(){
        final String firstname = studentRepository.getFirstnameByEmailAddress("joel@minlo.de");
        assertEquals(firstname, "JoJo");
    }

    @Test
    public void shouldReturnStudentByEmailAddressNative(){
        final Student addressNative = studentRepository.getStudentByEmailAddressNative("jojo@minlo.com");
        assertEquals(addressNative.getEmailId(), "jojo@minlo.com");
    }

    @Test
    public void shouldReturnStudentByEmailAddressNativeParam(){
        final Student addressNative = studentRepository.getStudentByEmailAddressNativeWithParam("jojo@minlo.com");
        assertEquals(addressNative.getEmailId(), "jojo@minlo.com");
    }

    @Test
    public void shouldUpdateStudentNameWithParamAndTransaction(){
        studentRepository.updatedStudentNameByEmail("David", "joel@minlo.de");

    }

}