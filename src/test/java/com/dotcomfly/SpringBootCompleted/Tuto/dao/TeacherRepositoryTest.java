package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Course;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void saveTeacher(){

        Course math = Course.builder()
                .title("Math")
                .credit(5)
                .build();

        Course physique = Course.builder()
                .title("Math")
                .credit(5)
                .build();

        Teacher jojo = Teacher.builder()
                .email("jojo@minlo.de")
                .fisrtname("JoJo")
                .lastname("Minlo")
                //.courses(List.of(math, physique))
                .build();

        teacherRepository.save(jojo);


    }

}