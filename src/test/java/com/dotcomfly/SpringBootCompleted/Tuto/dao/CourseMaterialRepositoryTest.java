package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Course;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.CourseMaterial;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseMaterialRepositoryTest {

    @Autowired
    private CourseMaterialRepository courseMaterialRepository;

    @Test
    public void saveCourseMaterial(){
        //first create a course to save
        Course course = Course.builder()
                .title("Spring-Boot-MasterClass")
                .credit(10)
                .build();
        // first create a Course to save
        CourseMaterial courseMaterial = CourseMaterial.builder()
                .url("https://www.youtube.com/watch?v=zvR-Oif_nxg&t=15704s")
               // .course(course)
                .build();
        // save courseMaterial in DB
        courseMaterialRepository.save(courseMaterial);
    }

}