package com.dotcomfly.SpringBootCompleted.Tuto.controller;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import com.dotcomfly.SpringBootCompleted.Tuto.error.DepartmentNotFoundException;
import com.dotcomfly.SpringBootCompleted.Tuto.service.IDepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IDepartmentService departmentService;

    private Department department;

    @BeforeEach
    void setUp() {
        department = Department.builder()
                .departmentId(1L)
                .departmentName("IT-Service")
                .departmentAddress("MusterStr 55")
                .departmentCode("33333")
                .build();
    }

    @Test
    @DisplayName("save Department in DB")
    public void shouldSaveDepartment() throws Exception {
        Department inputdepartment = Department.builder()
                .departmentName("IT-Service")
                .departmentAddress("MusterStr 55")
                .departmentCode("33333")
                .build();

        Mockito.when(departmentService.saveDepartment(inputdepartment)).thenReturn(department);
        mvc.perform(post("/department/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"departmentName\":\"IT-Service\",\n" +
                        "    \"departmentAddress\":\"MusterStr 55\",\n" +
                        "    \"departmentCode\":\"33333\"\n" +
                        "}"))
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("Return Department By Id if exist")
    public void shouldReturnDepartmentById() throws Exception {
        Mockito.when(departmentService.getDepartmentById(1L)).thenReturn(department);
        mvc.perform(get("/department/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.departmentName").value(department.getDepartmentName()));



    }
}