package com.dotcomfly.SpringBootCompleted.Tuto.service;

import com.dotcomfly.SpringBootCompleted.Tuto.dao.IDepartmentRepository;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import com.dotcomfly.SpringBootCompleted.Tuto.error.DepartmentNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IDepartmentServiceTest {

    @Autowired
    private IDepartmentService departmentService;

    @MockBean
    private IDepartmentRepository departmentRepository;

    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentName("Bank-Verlag")
                .departmentAddress("FreeStreet 2")
                .departmentCode("5000")
                .departmentId(1L)
                .build();
        // when test
        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("Bank-Verlag")).thenReturn(department);
        // 
        Mockito.when(departmentRepository.findById(1L)).thenReturn(Optional.of(department));
        // save
        departmentRepository.save(department);
        Mockito.when(departmentRepository.save(department)).thenReturn(department);
    }

    @Test
    @DisplayName("Get DepartmentByName if Exist")
    public void shouldReturnDepartmentByNameIfDepartmentExist(){
        // input departmentName
        String departmentName = "Bank-Verlag";
        final Department departmentFound = departmentService.getDepartmentByName(departmentName);

        assertEquals(departmentName, departmentFound.getDepartmentName());
    }

    @Test
    @DisplayName("Get Department by Id if Exist")
    public void shouldReturnDepartmentByIdIfDepartmentExist() throws DepartmentNotFoundException {
        // input ID
        Long departmentId = 1L;
        final Department departmentById = departmentService.getDepartmentById(departmentId);

        assertEquals(departmentId, departmentById.getDepartmentId());
    }

//    @Test
//    @DisplayName("Save the department in DB")
//    public void shouldSaveDepartmentInDBIfTheDepartmentDontExistInDB(){
//        //create new department
//        Department newDepartment = new Department();
//        newDepartment.setDepartmentId(1L);
//        newDepartment.setDepartmentName("IT-Solutions");
//        newDepartment.setDepartmentAddress("GuteStr 2");
//        newDepartment.setDepartmentCode("3000");
//
//        final Department departmentToSave = departmentService.saveDepartment(newDepartment);
//
//        assertEquals(newDepartment, departmentToSave.getDepartmentId());
//        assertEquals(newDepartment, departmentToSave.getDepartmentName());
//        assertEquals(newDepartment, departmentToSave.getDepartmentAddress());
//        assertEquals(newDepartment, departmentToSave.getDepartmentCode());
//    }
}