package com.dotcomfly.SpringBootCompleted.Tuto.service;

import com.dotcomfly.SpringBootCompleted.Tuto.dao.PasswordResetTokenRepository;
import com.dotcomfly.SpringBootCompleted.Tuto.dao.UserRepository;
import com.dotcomfly.SpringBootCompleted.Tuto.dao.VerificationTokenRepository;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.PasswordResetToken;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.User;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.VerificationToken;
import com.dotcomfly.SpringBootCompleted.Tuto.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerUser(UserModel userModel) {
        User user = new User();
        user.setEmail(userModel.getEmail());
        user.setFirstname(userModel.getFirstname());
        user.setLastname(userModel.getLastname());
        user.setPassWord(passwordEncoder.encode( userModel.getPassWord()));
        user.setRole("USER");

        userRepository.save(user);
        return user;
    }

    @Override
    public void saveVerificationTokenForUser(String token, User user) {

        VerificationToken verificationToken = new VerificationToken(user, token);

        verificationTokenRepository.save(verificationToken);


    }

    @Override
    public String validateVerificationToken(String token) {
        final VerificationToken currentToken = verificationTokenRepository.findByToken(token);

        if (currentToken == null){
            return "Invalid Token";
        }

        User user = currentToken.getUser();
        Calendar calendar = Calendar.getInstance();

        if ((currentToken.getExpirationTime().getTime() - calendar.getTime().getTime()) <= 0){
            verificationTokenRepository.delete(currentToken);
            return "Token expired";
        }

        user.setEnabled(true);
        userRepository.save(user);


        return "valid";
    }

    @Override
    public VerificationToken generateNewVerificationToken(String oldToken) {
        final VerificationToken verificationToken = verificationTokenRepository.findByToken(oldToken);
        verificationToken.setToken(UUID.randomUUID().toString());
        verificationTokenRepository.save(verificationToken);
        return verificationToken;
    }

    @Override
    public User findUserByEmail(String email) {
         return userRepository.findByEmail(email);
    }

    @Override
    public void createPasswordResetTokenForUser(User userByEmail, String newToken) {

        PasswordResetToken passwordResetToken = new PasswordResetToken(userByEmail, newToken);

        passwordResetTokenRepository.save(passwordResetToken);
    }

    @Override
    public String validatePasswordResetToken(String token) {

        final PasswordResetToken passwordResetToken = passwordResetTokenRepository.findByToken(token);

        if (passwordResetToken == null){
            return "Invalid Token";
        }

        User user = passwordResetToken.getUser();
        Calendar calendar = Calendar.getInstance();

        if ((passwordResetToken.getExpirationTime().getTime() - calendar.getTime().getTime()) <= 0){
            passwordResetTokenRepository.delete(passwordResetToken);
            return "Token expired";
        }

        return "valid";
    }

    @Override
    public Optional<User> getUserByPasswordResetToken(String token) {
        return Optional.ofNullable(passwordResetTokenRepository.findByToken(token).getUser());
    }

    @Override
    public void changePassword(User user, String newPassword) {
        user.setPassWord(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }
}
