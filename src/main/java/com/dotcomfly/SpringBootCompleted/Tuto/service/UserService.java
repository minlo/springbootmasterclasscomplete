package com.dotcomfly.SpringBootCompleted.Tuto.service;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.User;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.VerificationToken;
import com.dotcomfly.SpringBootCompleted.Tuto.model.UserModel;

import java.util.Optional;

public interface UserService {

    User registerUser(UserModel userModel);

    void saveVerificationTokenForUser(String token, User user);

    String validateVerificationToken(String token);

    VerificationToken generateNewVerificationToken(String oldToken);

    User findUserByEmail(String email);

    void createPasswordResetTokenForUser(User userByEmail, String newToken);

    String validatePasswordResetToken(String token);

    Optional<User> getUserByPasswordResetToken(String token);

    void changePassword(User user, String newPassword);
}
