package com.dotcomfly.SpringBootCompleted.Tuto.service;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import com.dotcomfly.SpringBootCompleted.Tuto.error.DepartmentNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IDepartmentService {

    public Department saveDepartment(Department department);

    public List<Department> getAllTheDepartmentInOurDB();

    public Department getDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public Department getDepartmentByName(String departmentName);

}
