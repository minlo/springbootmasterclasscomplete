package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface IStudentRepository extends JpaRepository<Student, Long> {

    //Without JPQL Annotations
    public List<Student> findByFirstname(String firstname);

    public List<Student> findByFirstnameContaining(String name);

    public List<Student> findBylastnameNotNull();

    public List<Student> findByGuardianName(String guardianname);

    //Student findBYFirstnameAndLastname(String firstname, String lastname);

    //With JPQL Annotations
    @Query("select s from Student s where s.emailId = ?1")
    Student getStudentByEmailAddress(String email);

    @Query("select s.firstname from Student s where s.emailId = ?1")
    String getFirstnameByEmailAddress(String email);

    //Native Query
    @Query(
            value = "SELECT * FROM spgboot_master_class.student_table s where s.email_address = ?1",
            nativeQuery = true)
    Student getStudentByEmailAddressNative(String email);

    //Query with name Parameter
    @Query(
            value = "SELECT * FROM spgboot_master_class.student_table s where s.email_address = :email",
            nativeQuery = true)
    Student getStudentByEmailAddressNativeWithParam(@Param("email") String email);


    @Modifying
    @Transactional
    @Query(
            value = "update student_table set firstname = ?1 where email_address = ?2",
            nativeQuery = true
    )
    int updatedStudentNameByEmail(String firstname, String emil);



}
