package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
}
