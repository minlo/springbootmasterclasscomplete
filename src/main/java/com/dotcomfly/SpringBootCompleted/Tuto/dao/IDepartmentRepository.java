package com.dotcomfly.SpringBootCompleted.Tuto.dao;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDepartmentRepository extends JpaRepository<Department, Long> {

    //Standard JPA approach
    public Department findByDepartmentName(String departmentName);

    //Standard JPA IgnoreCase Sensitive approach
    public Department findByDepartmentNameIgnoreCase(String departmentName);
}
