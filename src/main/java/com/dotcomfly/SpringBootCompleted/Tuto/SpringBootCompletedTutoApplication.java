package com.dotcomfly.SpringBootCompleted.Tuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCompletedTutoApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBootCompletedTutoApplication.class, args);
	}

}



