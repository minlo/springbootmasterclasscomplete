package com.dotcomfly.SpringBootCompleted.Tuto.controller;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.Department;
import com.dotcomfly.SpringBootCompleted.Tuto.error.DepartmentNotFoundException;
import com.dotcomfly.SpringBootCompleted.Tuto.service.IDepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    //Logger peut nous aider lors du debuggage pour savoir on se trouve dans le programme
    private final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    @PostMapping("/add")
    public Department saveDepartment(@Valid @RequestBody Department department){
        LOGGER.info("Inside saveDepartment() of DepartmentController");
       return departmentService.saveDepartment(department);
    }

    @GetMapping("/all")
    public List<Department> getAllTheDepartmentInOurDB(){
        LOGGER.info("Inside getAllTheDepartmentInOurDB() of DepartmentController");
        return departmentService.getAllTheDepartmentInOurDB();
    }

    @GetMapping("/{departmentID}")
    public Department getDepartmentById(@PathVariable ("departmentID") Long departmentId) throws DepartmentNotFoundException {
        return departmentService.getDepartmentById(departmentId);
    }

    @DeleteMapping("/{departmentId}")
    public String deleteDepartmentById(@PathVariable("departmentId") Long departmentId) throws DepartmentNotFoundException {
        final Department departmentByIdToDelete = getDepartmentById(departmentId);
        if (departmentByIdToDelete != null){
            departmentService.deleteDepartmentById(departmentId);
            return "Department with ID "+departmentId+" is deleted";
        }else {
            return  "Department wuith ID "+departmentId+" don't exist in our DB";
        }

    }

    @PutMapping("/{departmentId}")
    public Department updateDepartment(
            @PathVariable ("departmentId") Long departmentId,
            @RequestBody Department department){

        return departmentService.updateDepartment(departmentId, department);
    }

    @GetMapping("/name/{name}")
    public Department getDepartmentByName(@PathVariable("name") String departmentName){
        return departmentService.getDepartmentByName(departmentName);
    }



}
