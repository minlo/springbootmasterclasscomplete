package com.dotcomfly.SpringBootCompleted.Tuto.controller;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.User;
import com.dotcomfly.SpringBootCompleted.Tuto.entity.VerificationToken;
import com.dotcomfly.SpringBootCompleted.Tuto.event.RegistrationCompleteEvent;
import com.dotcomfly.SpringBootCompleted.Tuto.model.PasswordModel;
import com.dotcomfly.SpringBootCompleted.Tuto.model.UserModel;
import com.dotcomfly.SpringBootCompleted.Tuto.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("security")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("/userRegister")
    public String registerUser(@RequestBody UserModel userModel, final HttpServletRequest request){

        final User user = userService.registerUser(userModel);
        applicationEventPublisher.publishEvent(new RegistrationCompleteEvent(user, applicationUrl(request)));
        return "Success";
    }

    @GetMapping("/verifyRegistration")
    public String verifyRegistration(@RequestParam("token") String token){
        String currentToken = userService.validateVerificationToken(token);
        if (currentToken.equalsIgnoreCase("valid")){
            return "User verifies successfully";
        }
        else {
            return "Bad User";
        }
    }

    private String applicationUrl(HttpServletRequest request) {
        return "http://"+
                request.getServerName()+
                ":"+
                request.getServerPort()+
                request.getContextPath();
    }

    @PostMapping("/resetPassword")
    public String resetPassword(@RequestBody PasswordModel passwordModel, HttpServletRequest request){

        final User userByEmail = userService.findUserByEmail(passwordModel.getEmail());

        String url = "";

        if (userByEmail != null){
            //create new token
            String newToken = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(userByEmail, newToken);
            url = passwordResetTokenMail(userByEmail, applicationUrl(request), newToken);
        }


        return url;
    }

    @PostMapping("/savePassword")
    public String savePassword(
            @RequestParam("token") String token,
            @RequestBody PasswordModel passwordModel){

        String result = userService.validatePasswordResetToken(token);
        if (!result.equalsIgnoreCase("valid")){
            return "Invalid Token";
        }
        Optional<User> user = userService.getUserByPasswordResetToken(token);
        if (user.isPresent()) {
            userService.changePassword(user.get(), passwordModel.getNewPassword());
            return "Password reset successfully";
        }else {
            return "Invalid Token";
        }
    }

    private String passwordResetTokenMail(User userByEmail, String applicationUrl, String newToken) {

        //Send Email to User
        String url = applicationUrl
                + "/security/savePassword?token="
                + newToken;
        //sendVerificationEmail()
        log.info("Click the link below to reset your Password: {}",
                url);
        return url;
    }

    @GetMapping("/resendVerifyToken")
    public String resendVerificationToken(@RequestParam("token") String oldToken, HttpServletRequest request){

        final VerificationToken verificationToken = userService.generateNewVerificationToken(oldToken);

        final User user = verificationToken.getUser();

        resendVerificationTokenMail(user, applicationUrl(request), verificationToken);
        
        return "Verification Link sent";
    }

    private void resendVerificationTokenMail(User user, String applicationUrl, VerificationToken verificationToken) {

        //Send Email to User
        String url = applicationUrl
                + "/security/verifyRegistration?token="
                + verificationToken.getToken();
        //sendVerificationEmail()
        log.info("Click the link to verify your account: {}",
                url);
    }

}
