package com.dotcomfly.SpringBootCompleted.Tuto.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HalloController {



    @GetMapping("/hallo")
    public String sayHallo(){
        return "Bonjour Joseph, Comment vas-tu??";
    }
}
