package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "TEACHER_TABLE")
public class Teacher {

    @Id
    @SequenceGenerator(
            name = "teacher_sequence",
            sequenceName = "teacher_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacher_sequence")
    @Column(name = "TEACHER_ID")
    private Long teacherId;

    @Column(name = "FIRSTNAME")
    private String fisrtname;

    @Column(name = "LASTNAME")
    private String lastname;

    @Column(name = "ADDRESS_EMAIL")
    private String email;

    @ToString.Exclude
    @OneToMany(mappedBy = "teacher", fetch = FetchType.EAGER)
    private List<Course> courses;

}
