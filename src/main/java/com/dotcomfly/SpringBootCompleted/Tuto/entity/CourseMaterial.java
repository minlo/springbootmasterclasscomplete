package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "COURSE_MATERIAL_TABLE")
public class CourseMaterial {

    @Id
    @SequenceGenerator(
            name = "course_Material_sequence",
            sequenceName = "course_Material_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course_Material_sequence")
    @Column(name = "COURSE_MATERIAL_ID")
    private Long course_material_Id;

    @Column(name = "URL")
    private String url;

    @ToString.Exclude
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(
            name = "COURSE_ID",
            referencedColumnName = "COURSE_ID"
    )
    private Course course;


}
