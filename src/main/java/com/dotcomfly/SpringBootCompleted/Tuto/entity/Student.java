package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(
        name = "STUDENT_TABLE",
        uniqueConstraints = @UniqueConstraint(
                name = "emailID_Unique",
                columnNames = "EMAIL_ADDRESS"
        )
)
public class Student {

    @Id
    @SequenceGenerator(
            name = "studend_sequence",
            sequenceName = "studend_sequence",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "studend_sequence")
    @Column(name = "STUDENT_ID")
    private Long studendId;

    @Column(name = "FIRSTNAME")
    private String firstname;

    @Column(name = "LASTNAME")
    private String lastname;

    @Column(name = "EMAIL_ADDRESS", nullable = false)
    private String emailId;

    @Embedded
    private Guardian guardian;

    //@ToString.Exclude
//    @ManyToMany( cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "STUDENT_HAS_COURSE",
//            joinColumns = @JoinColumn(name = "STUDENT_ID"),
//            inverseJoinColumns = @JoinColumn(name = "COURSE_ID"))
//    private List<Course> courses;

}
