package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "GUARDIAN_NAME")),
        @AttributeOverride(name = "email", column = @Column(name = "GUARDIAN_EMAIL")),
        @AttributeOverride(name = "mobile", column = @Column(name = "GUARDIAN_MOBILE"))
})
public class Guardian {

    private String name;
    private String email;
    private String mobile;
}
