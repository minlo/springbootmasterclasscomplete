package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "VERIFICATION_TABLE")
public class VerificationToken {

    //Expiration Time 10 minutes
    private static final int EXPIRATION_TIME = 10;

    @Id
    @SequenceGenerator(
            name = "Verification_sequence",
            sequenceName = "Verification_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Verification_sequence")
    @Column(name = "TOKEN_ID")
    private Long iD;

    @Column(name = "TOKEN_CONTENT")
    private String token;

    @Column(name = "EXPIRATION_TIME")
    private Date expirationTime;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(
            name = "USER_ID", nullable = false,
            foreignKey = @ForeignKey(name = "FK_VERIFICATION_TOKEN"))
    private User user;

    public VerificationToken(User user, String token){
        super();
        this.user = user;
        this.token = token;
        this.expirationTime = calculateExpirationDate(EXPIRATION_TIME);
    }
    public VerificationToken(String token){
        super();
        this.token = token;
        this.expirationTime = calculateExpirationDate(EXPIRATION_TIME);
    }

    private Date calculateExpirationDate(int expirationTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(new Date().getTime());
        calendar.add(Calendar.MINUTE, EXPIRATION_TIME);
        return new Date(calendar.getTime().getTime());
    }
}
