package com.dotcomfly.SpringBootCompleted.Tuto.entity.listener;

import com.dotcomfly.SpringBootCompleted.Tuto.entity.User;
import com.dotcomfly.SpringBootCompleted.Tuto.event.RegistrationCompleteEvent;
import com.dotcomfly.SpringBootCompleted.Tuto.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class RegistrationCompleteEventListener implements ApplicationListener<RegistrationCompleteEvent> {

    @Autowired
    private UserService userService;


    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        //Create the verification Token for the User with Link
        final User user = event.getUser();
        final String token = UUID.randomUUID().toString();
        userService.saveVerificationTokenForUser(token, user);

        //Send Email to User
        String url = event.getApplicationUrl()
                + "/verifyRegistration?token="
                + token;
        //sendVerificationEmail()
        log.info("Click the link to verify your account: {}",
                url);
    }
}
