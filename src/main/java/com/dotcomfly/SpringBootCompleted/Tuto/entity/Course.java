package com.dotcomfly.SpringBootCompleted.Tuto.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "COURSE_TABLE")
public class Course {

    @Id
    @SequenceGenerator(
            name = "course_sequence",
            sequenceName = "course_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course_sequence")
    @Column(name = "COURSE_ID")
    private Long courseId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CREDIT")
    private Integer credit;

    @ToString.Exclude
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(
            name = "TEACHER_ID",
            referencedColumnName = "TEACHER_ID"
    )
    private Teacher teacher;

    @ToString.Exclude
    @OneToOne(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private CourseMaterial courseMaterial;

    @ToString.Exclude
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "STUDENT_HAS_COURSE",
            joinColumns = @JoinColumn(
                    name = "COURSE_ID",
                    referencedColumnName = "COURSE_ID"
            ), inverseJoinColumns = @JoinColumn(
                    name = "STUDENT_ID",
            referencedColumnName = "STUDENT_ID")
    )
    private List<Student> students;

    public void addStudent(Student student){

        if (students == null){
            students = new ArrayList<>();
            students.add(student);
        }
    }


}
