package com.dotcomfly.SpringBootCompleted.Tuto.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor // Default constructor
@AllArgsConstructor // Constructor with param
@Builder            //
@Table(name = "DEPARTMENTO")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DEPARTMENTo_ID")
    private Long departmentId;

    /** Es gibt viele Validationsmöglichkeiten Beispiel:
     * @NotBlank(message = " ....")
     * @Length(max= 5, min = 1)
     * @Size(max = 10, min = 0)
     * @Email
     * @Positive
     * @Negative
     * @PositiveOrZero
     * @NegativeOrZero
     * @Future
     * @FutureOrPresent
     * @Past
     * @PastOrPresent
     */
    @NotBlank(message = "Please add the Name of the Department")
    @Column(name = "DEPARTMENT_NAME")
    private String departmentName;

    @Column(name = "DEPARTMENT_ADDRESS")
    private String departmentAddress;

    @Column(name = "DEPARTMENT_CODE")
    private String departmentCode;

}
